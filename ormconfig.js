module.exports = [
   {
      "name": "production",
      "type": "mysql",
      "host": process.env.DB_HOST,
      "port": process.env.DB_PORT,
      "username": process.env.DB_USER,
      "password": process.env.DB_PASSWORD,
      "database": process.env.DB_DATABASE,
      "synchronize": true,
      "logging": false,
      "entities": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/models/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "migrations": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "cli": {
         "entitiesDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/models`,
         "migrationsDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations`
      }
   },
   {
      "name": "development",
      "type": "mysql",
      "host": process.env.DB_HOST,
      "port": process.env.DB_PORT,
      "username": process.env.DB_USER,
      "password": process.env.DB_PASSWORD,
      "database": process.env.DB_DATABASE,
      "synchronize": true,
      "logging": false,
      "entities": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/models/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "migrations": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "cli": {
         "entitiesDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/models`,
         "migrationsDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations`
      }
   },
   {  
      "name": "testing",
      "type": "mysql",
      "host": process.env.DB_HOST,
      "port": process.env.DB_PORT,
      "username": process.env.DB_USER,
      "password": process.env.DB_PASSWORD,
      "database": process.env.DB_DATABASE,
      "synchronize": true,
      "logging": false,
      "dropSchema": true,
      "entities": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/models/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "migrations": [
         `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations/*.${process.env.TYPE_ORM_FILE_PREFIX}`
      ],
      "cli": {
         "entitiesDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/models`,
         "migrationsDir": `${process.env.TYPE_ORM_BUILD_PREFIX}src/db/migrations`
      } 
   }
]
