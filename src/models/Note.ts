import { Column, CreateDateColumn, Entity,
    getRepository, Like, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity('notes')
export class Note {
  @PrimaryGeneratedColumn()
    id: number;

  @Column({ type: 'integer', nullable: true })
    userId: number;

  @Column({ type: 'varchar', nullable: true })
    title: string;

  @Column({ type: 'text', nullable: true })
    content: string;

  @Column({ type:'varchar', nullable: true })
    ipAddress: string;

  @Column()
    @CreateDateColumn()
    createdAt: Date;

  @Column()
    @UpdateDateColumn()
    updatedAt: Date;

  constructor(data: any = {}) {
    if (!data) {
      return;
    }

    this.id = data.id;
    this.userId = data.userId;
    this.title = data.title;
    this.content = data.content;
    this.ipAddress = data.ipAddress;
  }

  async all(query: any) {
    try {
      return await getRepository(Note).find(
        {
          where: {
            title: Like(`%${query.sort ? query.sort : ''}%`),
            userId: query.userId,
          },
          order: { createdAt: query.order },
          take: query.take,
          skip: query.page * query.take,
        },
            );
    } catch (error) {
      console.log(error);
      throw new Error('ERROR');
    }
  }

  async findOne(id: number) {
    try {
      const result = await getRepository(Note).findOneOrFail(id);
      if (!result) return {};
      this.constructor(result);
    } catch (error) {
      console.log(error);
      throw new Error('ERROR');
    }
  }

  async store() {
    try {
      return await getRepository(Note).save(this);
    } catch (error) {
      console.log(error);
      throw new Error('ERROR');
    }
  }

  async destroy() {
    try {
      return await getRepository(Note).delete(this.id);
    } catch (error) {
      console.log(error);
      throw new Error('ERROR');
    }
  }

}
