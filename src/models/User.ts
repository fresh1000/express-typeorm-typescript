import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar' })
  token: string;

  @Column({ type: 'varchar' })
  firstName: string;

  @Column({ type: 'varchar' })
  lastName: string;

  @Column({ type: 'varchar', unique: true, length: 191 })
  username: string;

  @Column({ type: 'varchar', unique: true, length: 191 })
  email: string;

  @Column({ type: 'varchar' })
  password: string;

  @Column({ type: 'varchar', nullable: true })
  passwordResetToken: string;

  @Column({ type: 'timestamp', nullable: true })
  passwordResetExpiration: Date;

  @Column({ type: 'boolean', default: false })
  sendPromotionalEmails: boolean;

  @Column({ type: 'boolean', default: false })
  isAdmin: boolean;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @Column({ type:'int', default: 0, nullable: true })
  loginCount: number;

  @Column({ type:'varchar', nullable: true })
  ipAddress: string;

  @Column()
  @CreateDateColumn()
  createdAt: Date;

  @Column()
  @UpdateDateColumn()
  updatedAt: Date;

  constructor(data: any = {}) {
    if (!data) {
      return;
    }

    this.id = data.id;
    this.token = data.token;
    this.username = data.username;
    this.email = data.email;
    this.isAdmin = data.isAdmin;
  }
}
