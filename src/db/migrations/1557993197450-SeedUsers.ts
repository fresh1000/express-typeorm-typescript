import * as bcrypt from 'bcryptjs';
import * as faker from 'faker';
import { getRepository, MigrationInterface, QueryRunner } from 'typeorm';
import { User } from '../../models/User';

export class SeedUsers1557993197450 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    const userRepository = getRepository(User);
    const seedData = [];
    for (let i = 0; i < 5; i += 1) {
      let password = 'demopassword';
      try {
        password = await bcrypt.hash(password, 12);
      } catch (error) {
        throw new Error('PASSWORD_ENCRIPTION_ERROR');
      }

      if (i === 0) {
        const testUser = {
          token: 'qwertyuiop',
          firstName: 'DemoFirstName',
          lastName: 'DemoLastName',
          username: 'demousername',
          email: 'demoemail@example.com',
          password,
        };
        seedData.push(testUser);
        continue;
      }

      const testUser = {
        token: faker.internet.password(),
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        username: faker.internet.userName(),
        email: faker.internet.email(),
        password,
      };

      seedData.push(testUser);
    }

    await userRepository.save(seedData);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('DELETE FROM users');
  }

}
