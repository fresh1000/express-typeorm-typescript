import { MigrationInterface, QueryRunner, getRepository } from 'typeorm';
import { Note } from '../../models/Note';
import * as faker from 'faker';

export class SeedNotes1557994933521 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    const noteRepository = getRepository(Note);
    const seedData = [];
    for (let i = 0; i < 100; i += 1) {
      const testNote = {
        userId: faker.random.number({ min: 1, max: 5 }),
        title: faker.lorem.sentence(),
        content: faker.lorem.sentences(Math.floor(Math.random() * 10) + 1),
      };
      seedData.push(testNote);
    }
    await noteRepository.save(seedData);

  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.query('DELETE FROM notes');
  }

}
