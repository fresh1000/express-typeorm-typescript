import { NextFunction, Request, Response } from 'express';
import * as jsonwebtoken from 'jsonwebtoken';

export default (opts: any = {}) => {
  const secret = opts.secret;

  const middleware = async function jwt(req: Request, res: Response, next: NextFunction) {
      // If there's no secret set, toss it out right away
    if (!secret) {
      return res.status(401).send('INVALID_SECRET');
    }

      // Grab the token
    const token = getJwtToken(req, res);
    try {
          // Try and decode the token asynchronously
      const decoded = await jsonwebtoken.verify(
              token,
              process.env.JWT_SECRET,
          );

          // If it worked set the req.state.user parameter to the decoded token.
      req.state = { user: decoded.data };
    } catch (error) {
      if (error.name === 'TokenExpiredError') {
        return res.status(401).send('TOKEN_EXPIRED');
      }
      return res.status(401).send('AUTHENTICATION_ERROR');

    }

    return next();
  };

  function getJwtToken(req: Request, res: Response) {
    if (!req.header || !req.headers.authorization) {
      return;
    }

    const parts = req.headers.authorization.split(' ');

    if (parts.length === 2) {
      const scheme = parts[0];
      const credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        return credentials;
      }
    }
    return res.status(401).send('AUTHENTICATION_ERROR');
  }

  return middleware;
};
