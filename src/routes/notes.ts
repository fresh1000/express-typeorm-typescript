import { Router } from 'express';
import jwt from '../middleware/jwt';
import NoteController from '../controllers/NoteController';

const router = Router();
const jwtMiddleware = jwt({ secret: process.env.JWT_SECRET });
const noteController = new NoteController();

router.get('/api/v1/notes', jwtMiddleware, noteController.index);

router.get('/api/v1/notes/:id', jwtMiddleware, noteController.findOne);

router.post('/api/v1/notes', jwtMiddleware, noteController.create);

router.delete('/api/v1/notes/:id', jwtMiddleware, noteController.delete);

router.put('/api/v1/notes/:id', jwtMiddleware, noteController.update);

export default router;
