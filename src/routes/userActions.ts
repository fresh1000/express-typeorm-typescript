import { Request, Response, Router } from 'express';
import UserActionController from '../controllers/UserActionController';
import jwt from '../middleware/jwt';

const router = Router();
const jwtMiddleware = jwt({ secret: process.env.JWT_SECRET });

router.get('/', (req: Request, res: Response) => {
  res.send({ message: `Hi there. ${process.env.npm_package_version}` });
});

// Initial controller once for all routes
const userActionController = new UserActionController();

router.post('/api/v1/user/signup', async (req: Request, res: Response) => {
  await userActionController.signup(req, res);
});

router.post('/api/v1/user/authenticate', userActionController.authenticate);

router.post('/api/v1/user/refreshAccessToken', userActionController.refreshAccessToken);

router.post('/api/v1/user/invalidateAllRefreshTokens',
            jwtMiddleware, userActionController.invalidateAllRefreshTokens);

router.post('/api/v1/user/invalidateRefreshToken',
            jwtMiddleware, userActionController.invalidateRefreshToken);

router.post('/api/v1/user/forgot', userActionController.forgot);

router.post('/api/v1/user/checkPasswordResetToken', userActionController.checkPasswordResetToken);

router.post('/api/v1/user/resetPassword', userActionController.resetPassword);

router.post('/api/v1/user/private', jwtMiddleware, userActionController.private);

export default router;
