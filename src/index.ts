import * as express from 'express';
import * as cors from 'cors';
import * as userAgent from 'express-useragent';
import * as bodyParser from 'body-parser';
import notes from './routes/notes';
import userActions from './routes/userActions';
import * as responseTime from 'response-time';
import log from './logs/log';

const app = express();

app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.on('finish', () => {
    log.info(`${req.method} ${req.originalUrl} RESPONSE: ${res.statusCode}`);
  });
  next();
});

app.use(responseTime());
app.use(cors({ origin: '*' }));
app.use(userAgent.express());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(notes);
app.use(userActions);

export default app;
