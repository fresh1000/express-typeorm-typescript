import { Request, Response } from 'express';
import * as joi from 'joi';
import { Note } from '../models/Note';
import { User } from '../models/User';

const noteSchema = joi.object({
  id: joi.number().integer(),
  userId: joi
        .number()
        .integer()
        .required(),
  title: joi.string().required(),
  content: joi.string().required(),
  ipAddress: joi.string(),

});

class NoteController {
  async index(req: Request, res: Response) {
    const query: any = req.query;
    query.take = query.limit;

        // Attach logged in user
    const user: User = new User(req.state.user);
    query.userId = user.id;
    const note: Note = new Note();
    if (!query.order || !query.page || !query.limit) {
      res.status(400).send('INVALID_ROUTE_OPTIONS');
    }

    try {
      const results = <any>await note.all(query);
      return res.send(results);
    } catch (error) {
      return res.status(400).send(`INVALID_DATA ${error}`);
    }

  }

  async findOne(req: Request, res: Response) {
    const id: number = req.params.id;

    if (!id) res.status(400).send('INVALID_DATA');

    const note: Note = new Note();
    try {
      await note.findOne(id);
      return res.send(note);
    } catch (error) {
      return res.status(400).send(`INVALID_DATA ${error}`);
    }
  }

  async create(req: Request, res: Response) {
    const request: any = req.body;
    request.ipAddress = req.ip;
        // Attach logged in user
    const user: User = new User(req.state.user);
    request.userId = user.id;

    const note: Note = new Note(request);
    const validator: any = joi.validate(note, noteSchema);
    if (validator.error) res.status(400).send(validator.error.details[0].message);
    try {
      const result: Note = await note.store();
      return res.send(result);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

  }

  async update(req: Request, res: Response) {
    const id: number = req.params.id;
    const request = req.body;

    const note: Note = new Note();
    try {
      await note.findOne(id);
    } catch (err) {
      return res.status(400).send('INVALID_DATA');
    }

        // Grab the user //If it's not their note - error out
    const user: User = new User(req.state.user);
    if (note.userId !== user.id) {
      return res.status(400).send('INVALID_DATA');
    }

    request.ipAddress = req.ip;

    Object.keys(request).forEach((parameter, index) => {
      note[parameter] = request[parameter];
    });

    try {
      const result: Note = await note.store();
      return res.send(result);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }
  }

  async delete(req: Request, res: Response) {
    const id: number = req.params.id;
    if (!id) {
      return res.status(400).send('INVALID_DATA');
    }

    const note: Note = new Note();
    try {
      await note.findOne(id);
    } catch (err) {
      return res.status(400).send('INVALID_DATA');
    }

        // Grab the user //If it's not their note - error out
    const user: User = new User(req.state.user);
    if (note.userId !== user.id) {
      return res.status(400).send('INVALID_DATA');
    }

    try {
      await note.destroy();
      return res.send('SUCCESS');
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

  }

}

export default NoteController;
