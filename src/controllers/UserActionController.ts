import * as bcrypt from 'bcryptjs';
import * as dateAddMinutes from 'date-fns/add_minutes';
import * as dateAddMonths from 'date-fns/add_months';
import * as dateCompareAsc from 'date-fns/compare_asc';
import * as dateFormat from 'date-fns/format';
import { Request, Response } from 'express';
import * as joi from 'joi';
import * as jsonwebtoken from 'jsonwebtoken';
import * as rand from 'randexp';
import { getRepository } from 'typeorm';
import { RefreshToken } from '../models/RefreshToken';
import { User } from '../models/User';

const userSchemaSignup = joi.object({
  firstName: joi
        .string()
        .min(1)
        .max(25)
        .alphanum()
        .required(),
  lastName: joi
        .string()
        .min(1)
        .max(25)
        .alphanum()
        .required(),
  username: joi
        .string()
        .min(3)
        .max(100)
        .regex(/[a-zA-Z0-9@]/)
        .required(),
  email: joi
        .string()
        .email()
        .required(),
  password: joi
        .string()
        .min(8)
        .max(35)
        .required(),
});

const userSchemaResetPassword = joi.object({
  email: joi
        .string()
        .email()
        .required(),
  password: joi
        .string()
        .min(8)
        .max(35)
        .required(),
  passwordResetToken: joi.string().required(),
});

class UserController {
  constructor() {}

  async signup(req: Request, res: Response) {
    const request: any = req.body;

    const validator = joi.validate(request, userSchemaSignup);
    if (validator.error) {
      return res.status(400).send(validator.error.details[0].message);
    }

    const resultUsername: User = await getRepository(User).findOne({
      where: {
        username: request.username,
      },
    });

    if (resultUsername) {
      return res.status(400).send('DUPLICATE_USERNAME');
    }

    const resultEmail: User = await getRepository(User).findOne({
      where: {
        email: request.email,
      },
    });

    if (resultEmail) {
      return res.status(400).send('DUPLICATE_USERNAME');
    }

        // Now let's generate a token for this user
    request.token = await this.generateUniqueToken();

        // Ok now let's hash their password.
    try {
      request.password = await bcrypt.hash(request.password, 12);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    request.idAddress = req.ip;

    try {
      const user: User = await getRepository(User).save(request);
      return res.status(200).send({ message: 'SUCCESS', id: user.id });
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }
  }

  async authenticate(req: Request, res: Response) {
    const request: any = req.body;
    if (!request.username || !request.password) res.status(404).send('INVALID_DATA');

    const userData: User = await getRepository(User).findOne({
      where: {
        username: request.username,
      },
      select: ['id', 'token', 'username', 'email', 'password', 'isAdmin'],
    });

    if (!userData) {
      return res.status(401).send('INVALID_CREDENTIALS');
    }

    try {
      const correct: any = await bcrypt.compare(
                request.password,
                userData.password,
            );

      if (!correct) res.status(400).send('INVALID_CREDENTIALS');

    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    delete userData.password;

        // let refreshTokenData = new RefreshToken()
    const refreshTokenData = {
      username: userData.username,
      refreshToken: new rand(/[a-zA-Z0-9_-]{64}/).gen(),
      info: `${req.useragent.os} ${req.useragent.platform} ${req.useragent.browser}`,
      ipAddress: req.ip,
      expiration: dateAddMonths(new Date(), 1),
      isValid: true,
    };

    try {
      await getRepository(RefreshToken).save(refreshTokenData);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    try {
      await getRepository(User).increment({ id: userData.id }, 'loginCount', 1);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    const token: string = jsonwebtoken.sign(
            { data: userData },
            process.env.JWT_SECRET,
            { expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME },
        );

    return res.send({
      accessToken: token,
      refreshToken: refreshTokenData.refreshToken,
    });

  }

  async refreshAccessToken(req: Request, res: Response) {
    const request: any = req.body;
    if (!request.username || !request.refreshToken) {
      return res.status(401).send('NO_REFRESH_TOKEN');
    }

    const refreshTokenDatabaseData: RefreshToken = await getRepository(RefreshToken).findOne({
      where: {
        username: request.username,
        refreshToken: request.refreshToken,
        isValid: true,
      },
      select: ['username', 'refreshToken', 'expiration'],
    });

    if (!refreshTokenDatabaseData) {
      return res.status(400).send('INVALID_REFRESH_TOKEN');
    }
    const refreshAccessTokenIsValid: any = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            refreshTokenDatabaseData.expiration,
        );

    if (refreshAccessTokenIsValid !== -1) {
      return res.status(400).send('REFRESH_TOKEN_EXPIRED');
    }

    try {
      await getRepository(RefreshToken).update({
        refreshToken: refreshTokenDatabaseData.refreshToken,
      },                                       { isValid: false });
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    const userData: User = await getRepository(User).findOne({
      where: {
        username: request.username,
      },
      select: ['id', 'token', 'username', 'email', 'isAdmin'],
    });

    if (!userData) {
      return res.status(401).send('INVALID_REFRESH_TOKEN');
    }

    const refreshTokenData = {
      username: request.username,
      refreshToken: new rand(/[a-zA-Z0-9_-]{64}/).gen(),
      info: `${req.useragent.os} ${req.useragent.platform} ${req.useragent.browser}`,
      ipAddress: req.ip,
      expiration: dateAddMonths(new Date(), 1),
      isValid: true,
    };

    try {
      await getRepository(RefreshToken).save(refreshTokenData);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    const token: string = jsonwebtoken.sign(
            { data: userData },
            process.env.JWT_SECRET,
            { expiresIn: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME },
        );

    return res.send({
      accessToken: token,
      refreshToken: refreshTokenData.refreshToken,
    });
  }

  async invalidateAllRefreshTokens(req: Request, res: Response) {
    const request: any = req.body;
    try {
      await getRepository(RefreshToken).update({
        username: request.username,
      },                                       {
        isValid: false,
      });
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }
    return res.send({ message: 'SUCCESS' });
  }

  async invalidateRefreshToken(req: Request, res: Response) {
    const request: any = req.body;
    if (!request.refreshToken) {
      return res.status(404).send('INVALID_DATA');
    }

    try {
      await getRepository(RefreshToken)
                .update({
                  username: req.state.user.username,
                  refreshToken: request.refreshToken,
                },      {
                  isValid: false,
                });
      return res.send({ message: 'SUCCESS' });
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }
  }

  async forgot(req: Request, res: Response) {
    const request: any = req.body;

    if (!request.email || !request.url || !request.type) {
      return res.status(404).send('INVALID_DATA');
    }

    const resetData = {
      passwordResetToken: new rand(/[a-zA-Z0-9_-]{64}/).gen(),
      passwordResetExpiration: dateAddMinutes(new Date(), 30),
    };

    try {
      await getRepository(User).findOneOrFail({ where: { email: request.email } });
      await getRepository(User).update({
        email: request.email,
      },                               resetData);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    if (request.type === 'web') {
            // let email = await fse.readFile('./src/email/forgot.html', 'utf8')
            // let resetUrlCustom: string =
            //     request.url +
            //     '?passwordResetToken=' +
            //     resetData.passwordResetToken +
            //     '&email=' +
            //     request.email

            // const emailData = {
            //     to: request.email,
            //     from: process.env.APP_EMAIL,
            //     subject: 'Password Reset For ' + process.env.APP_NAME,
            //     html: email,
            //     categories: ['koa-vue-notes-api-forgot'],
            //     substitutions: {
            //         appName: process.env.APP_NAME,
            //         email: request.email,
            //         resetUrl: resetUrlCustom,
            //     },
            // }
    }

    return res.send({ passwordResetToken: resetData.passwordResetToken });
  }

  async checkPasswordResetToken(req: Request, res: Response) {
    const request: any = req.body;

    if (!request.passwordResetToken || !request.email) {
      return res.status(404).send('INVALID_DATA');
    }

    let passwordResetData: User;
    try {
      passwordResetData = await getRepository(User).findOneOrFail({
        where: {
          email: request.email,
          passwordResetToken: request.passwordResetToken,
        },
        select: ['passwordResetExpiration'],
      });
    } catch (error) {
      return res.status(404).send('INVALID_TOKEN');
    }

    const tokenIsValid = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            passwordResetData.passwordResetExpiration,
        );

    if (tokenIsValid !== -1) {
      return res.status(400).send('RESET_TOKEN_EXPIRED');
    }

    return res.send({ message: 'SUCCESS' });
  }

  async resetPassword(req: Request, res: Response) {
    const request: any = req.body;

    const validator = joi.validate(request, userSchemaResetPassword);
    if (validator.error) {
      return res.status(400).send(validator.error.details[0].message);
    }
    let passwordResetData: User;
    try {
      passwordResetData = await getRepository(User).findOneOrFail({
        where: {
          email: request.email,
          passwordResetToken: request.passwordResetToken,
        },
        select: ['passwordResetExpiration'],
      });
    } catch (error) {
      return res.status(404).send('INVALID_TOKEN');
    }

    const tokenIsValid = dateCompareAsc(
            dateFormat(new Date(), 'YYYY-MM-DD HH:mm:ss'),
            passwordResetData.passwordResetExpiration,
        );
    if (tokenIsValid !== -1) {
      return res.status(400).send('RESET_TOKEN_EXPIRED');
    }

    try {
      request.password = await bcrypt.hash(request.password, 12);
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    request.passwordResetToken = null;
    request.passwordResetExpiration = null;
    try {
      await getRepository(User).update({
        email: request.email,
      },                               {
        password: request.password,
        passwordResetToken: request.passwordResetToken,
        passwordResetExpiration: request.passwordResetExpiration,
      });
    } catch (error) {
      return res.status(400).send('INVALID_DATA');
    }

    return res.send({ message: 'SUCCESS' });
  }

  async private(req: Request, res: Response) {
    return res.send({ user: req.state.user });
  }

  async generateUniqueToken() {
    const token: string = new rand(/[a-zA-Z0-9_-]{7}/).gen();
    if (await this.checkUniqueToken(token)) {
      await this.generateUniqueToken();
    } else {
      return token;
    }
  }

  async checkUniqueToken(token: string) {
    try {
      await getRepository(User).findOneOrFail({
        where: {
          token,
        },
      });
      return true;
    } catch (error) {
      return false;
    }
  }
}

export default UserController;
