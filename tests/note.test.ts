import * as request from 'supertest';
import { createConnection, getConnectionOptions } from 'typeorm';
import * as dotenv from 'dotenv';

dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

const app = require('../src/index').default;
let connection;
let accessToken: string;
let id: string;

beforeAll(async () => {
  const options = await getConnectionOptions(process.env.NODE_ENV);
  connection = await createConnection({ ...options, name: 'default' });
  await request(app)
      .post('/api/v1/user/signup')
      .send({
        firstName: 'TestFirstNameNote',
        lastName: 'TestLastNameNote',
        username: 'TestUsernameNote',
        email: 'TestEmailNote@example.com',
        password: 'TestPasswordNote',
      });
  await request(app)
    .post('/api/v1/user/authenticate')
    .send({ username: 'TestUsernameNote', password: 'TestPasswordNote' })
    .then((res) => {
      accessToken = res.body.accessToken;
    });
});

afterAll(async () => {
  connection.close();
});

describe('create', () => {
  afterAll(async () => {
    await request(app)
      .delete(`/api/v1/notes/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);
  });

  it('creates a note', (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/notes')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({
        title: 'Here is my first note create',
        content: 'Here is my main content create.',
      })
      .then((res) => {
        expect(res.status).toBe(200);
        id = res.body.id;
        done();
      });
  });
});

describe('show, update, show all', () => {
  beforeEach(async () => {
    await request(app)
        .post('/api/v1/notes')
        .set('Authorization', `Bearer ${accessToken}`)
        .send({
          title: 'Here is my first note',
          content: 'Here is my main content.',
        })
        .then((res) => {
          id = res.body.id;
        });
  });

  afterEach(async () => {
    await request(app)
      .delete(`/api/v1/notes/${id}`)
      .set('Authorization', `Bearer ${accessToken}`);
  });

  it('shows a note', (done) => {
    expect.assertions(3);
    request(app)
      .get(`/api/v1/notes/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.title).toBe('Here is my first note');
        expect(res.body.content).toBe('Here is my main content.');
        done();
      });
  });

  it("gets a bunch of a user's notes", (done) => {
    expect.assertions(4);
    request(app)
      .get('/api/v1/notes/')
      .set('Authorization', `Bearer ${accessToken}`)
      .query({ sort: '', order: 'desc', page: 0, limit: 20 })
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body[0].id).toBe(id);
        expect(res.body[0].title).toBe('Here is my first note');
        expect(res.body[0].content).toBe('Here is my main content.');
        done();
      });
  });

  it('updates a note', (done) => {
    expect.assertions(3);
    request(app)
      .put(`/api/v1/notes/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .send({
        title: 'Here is my first note',
        content: 'Here is my main content.',
      })
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.title).toBe('Here is my first note');
        expect(res.body.content).toBe('Here is my main content.');
        done();
      });
  });
});

describe('note delete action', () => {
  beforeAll(async () => {
    await request(app)
      .post('/api/v1/notes')
      .set('Authorization', `Bearer ${accessToken}`)
      .send({
        title: 'Here is my first note update',
        content: 'Here is my main content update.',
      })
      .then((res) => {
        id = res.body.id;
      });
  });

  it('deletes a note', (done) => {
    expect.assertions(1);
    request(app)
      .delete(`/api/v1/notes/${id}`)
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });
});
