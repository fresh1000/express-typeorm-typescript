import * as request from 'supertest';
import { createConnection, getConnectionOptions } from 'typeorm';
import * as dotenv from 'dotenv';

dotenv.config({ path: `.env.${process.env.NODE_ENV}` });

const app = require('../src/index').default;
let connection;

beforeAll(async () => {
  const options = await getConnectionOptions(process.env.NODE_ENV);
  connection = await createConnection({ ...options, name: 'default' });
});
afterAll(async () => {
  connection.close();
});

let accessToken: string;
let refreshToken: string;
let passwordResetToken: string;

describe('general actions', () => {
  it('returns homepage', (done) => {
    expect.assertions(1);
    request(app)
      .get('/')
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });
});

describe('user signs up', () => {
  it('signs up a user', (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/signup')
      .send({
        firstName: 'TestFirstName',
        lastName: 'TestLastName',
        username: 'TestUsername',
        email: 'TestEmail@example.com',
        password: 'TestPassword',
      })
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });
});

describe('user auth', () => {
  beforeAll(async () => {
    await request(app)
      .post('/api/v1/user/signup')
      .send({
        firstName: 'TestFirstNameAuth',
        lastName: 'TestLastNameAuth',
        username: 'TestUsernameAuth',
        email: 'TestEmailAuth@example.com',
        password: 'TestPasswordAuth',
      });
  });

  it('authenticates a user', (done) => {
    expect.assertions(3);
    request(app)
      .post('/api/v1/user/authenticate')
      .send({ username: 'TestUsernameAuth', password: 'TestPasswordAuth' })
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.accessToken).toBeDefined();
        expect(res.body.refreshToken).toBeDefined();
        done();
      });
  });
});

describe('user account actions', () => {
  beforeAll(async () => {
    await request(app)
      .post('/api/v1/user/signup')
      .send({
        firstName: 'TestFirstNameRefresh',
        lastName: 'TestLastNameRefresh',
        username: 'TestUsernameRefresh',
        email: 'TestEmailRefresh@example.com',
        password: 'TestPasswordRefresh',
      });
    await request(app)
      .post('/api/v1/user/authenticate')
      .send({ username: 'TestUsernameRefresh', password: 'TestPasswordRefresh' })
      .then((res) => {
        accessToken = res.body.accessToken;
        refreshToken = res.body.refreshToken;
      });
  });

  it("refresh user's accessToken", (done) => {
    expect.assertions(3);
    request(app)
      .post('/api/v1/user/refreshAccessToken')
      .send({ username: 'TestUsernameRefresh', refreshToken })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.accessToken).toBeDefined();
        expect(res.body.refreshToken).toBeDefined();
        done();
      });
  });

  it("invalidate all the user's refreshTokens", (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/invalidateAllRefreshTokens')
      .send({ username: 'TestUsernameRefresh' })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });

  it('invalidate specific refreshToken', (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/invalidateRefreshToken')
      .send({ refreshToken })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });
  it('return data from a authenticated route', (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/private')
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });

  it("forgot user's password", (done) => {
    expect.assertions(2);
    request(app)
      .post('/api/v1/user/forgot')
      .send({
        email: 'TestEmail@example.com',
        url: 'http://koa-vue-notes-api.com/user/reset',
        type: 'web',
      })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.body.passwordResetToken).toBeDefined();
        done();
      });
  });
});

describe('password reset actions', () => {
  beforeAll(async() => {
    await request(app)
      .post('/api/v1/user/signup')
      .send({
        firstName: 'TestFirstNameResetPass',
        lastName: 'TestLastNameResetPass',
        username: 'TestUsernameResetPass',
        email: 'TestEmailResetPass@example.com',
        password: 'TestPasswordResetPass',
      });
    await request(app)
      .post('/api/v1/user/authenticate')
      .send({ username: 'TestUsernameResetPass', password: 'TestPasswordResetPass' })
      .then((res) => {
        accessToken = res.body.accessToken;
        refreshToken = res.body.refreshToken;
      });
    await request(app)
      .post('/api/v1/user/forgot')
      .send({
        email: 'TestEmailResetPass@example.com',
        url: 'http://koa-vue-notes-api.com/user/reset',
        type: 'web',
      })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        passwordResetToken = res.body.passwordResetToken;
      });
  });

  it('checks password reset token', (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/checkPasswordResetToken')
      .send({ passwordResetToken, email: 'TestEmailResetPass@example.com' })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });

  it("reset user's password", (done) => {
    expect.assertions(1);
    request(app)
      .post('/api/v1/user/resetPassword')
      .send({
        email: 'TestEmailResetPass@example.com',
        passwordResetToken,
        password: 'TestPassword',
      })
      .set('Authorization', `Bearer ${accessToken}`)
      .then((res) => {
        expect(res.status).toBe(200);
        done();
      });
  });
});
