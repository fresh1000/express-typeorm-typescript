import * as dotenv from 'dotenv';
import { createConnection, getConnectionOptions } from 'typeorm';

if (process.env.DOTENV) {
  process.env.DOTENV
    .split(',')
    .filter(val => val)
    .forEach(envPath => dotenv.config({ path: envPath }));
}

const env = process.env.NODE_ENV || 'development';
const port = process.env.PORT || 4000;

require('babel-polyfill');
if (env === 'development') {
  require('babel-register');
}

const app = require('./src/index').default;

async function createDbConnection() {
  const options = await getConnectionOptions(env);
  return createConnection({ ...options, name: 'default' });
}

export const server = createDbConnection().then(() => {
  const server = app.listen(port);
  console.log(`Server running at ${port}`);
  console.log(`Running in ${process.env.NODE_ENV} v${process.env.npm_package_version}`);
  return server;
});
